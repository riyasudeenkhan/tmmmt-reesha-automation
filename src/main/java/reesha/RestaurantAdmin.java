package reesha;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import utility.PageBase;

public class RestaurantAdmin extends PageBase{
	Logger log = Logger.getLogger(RestaurantAdmin.class);
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Master')]")
	@CacheLookup
	public WebElement lnk_Master;
 
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Restaurants')]/../i")
	@CacheLookup
	public WebElement lnk_Restaurants;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Restaurant Admin')]")
	@CacheLookup
	public WebElement lnk_RestAdmin;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Activate')]")
	@CacheLookup
	public WebElement lnk_Activate;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Activated Successfully.')]")
	@CacheLookup
	public WebElement toast_Verify;
	
	@FindBy(how = How.XPATH, using = "//input[@name='mobNo']")
	@CacheLookup
	public WebElement txt_Mobile;
	
	@FindBy(how = How.XPATH, using = "//input[@name='mobNo']/following-sibling::span/button")
	@CacheLookup
	public WebElement btn_Search;
	
	@FindBy(how = How.XPATH, using = "//tbody[@class='loader-container']/tr[2]/td[2]/a")
	@CacheLookup
	public WebElement lnk_Pkid;
	
	
	public RestaurantAdmin(WebDriver driver)
	{
		super(driver);
 	}
 
	public void dummy() {
		System.out.println("dummy function >>>>>>>>");
		implicitWait(20);
		System.out.println("<<<<<<<< dummy function >>>>>>>>");
	}
	
	public void navigateToRestAdmin(){
		log.debug("navigateToRestAdmin: Start" + lnk_Master.getText());
		lnk_Master.click();
		implicitWait(10);
		try {
			Thread.sleep(10000);
			System.out.println("navigateToRestAdmin, Restaurants is displayed? "+lnk_Restaurants.isDisplayed());
			if(!lnk_Restaurants.isDisplayed()) {
				lnk_Master.click();
				Thread.sleep(10000);
			}
			lnk_Restaurants.click();
			implicitWait(5);
			lnk_RestAdmin.click();
		} catch (InterruptedException e) {			
			log.error("navigateToRestAdmin: " + e.getMessage());
		}
		log.debug("navigateToRestAdmin: End");
	}
	
	public void openRestAdminDetails(String sMobile) {
		log.debug("openRestAdminDetails: Start");
		implicitWait(20);
		//driver.findElement(By.xpath("//a[contains(text(),'"+sRestName+"')]")).click();
		txt_Mobile.sendKeys(sMobile);
		waitForPageLoad(2);
		btn_Search.click();
		waitForPageLoad(2);
		lnk_Pkid.click();
		waitForPageLoad(10);
		log.debug("openRestAdminDetails: End");
	}
	
	public Boolean activateRestAdmin() {
		log.debug("activateRestAdmin: Start");
		log.debug("activateRestAdmin, Activate is displayed? "+lnk_Activate.isDisplayed());
		lnk_Activate.click();
		//Verify isActivated
		implicitWait(20);
		if(toast_Verify.isDisplayed()) {
			log.info("Successfully Restaurant is Activated.");
		} else {
			log.error("Restaurant Activation Failed");
			return false;
		}
		return true;
	}
}
