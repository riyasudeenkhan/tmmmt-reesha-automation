package reesha;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import utility.PageBase;

public class Reesha_Login extends PageBase{
	Logger log = Logger.getLogger(Reesha_Login.class);
	@FindBy(how = How.NAME, using = "email")
	@CacheLookup
	public WebElement txtbx_UserName;
 
	@FindBy(how = How.NAME, using = "password")
	@CacheLookup
	public WebElement txtbx_Password;
 
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	@CacheLookup
	public WebElement btn_Login ;
 
	public Reesha_Login(WebDriver driver)
	{
		super(driver);
 	}
 
	// This method will take two arguments ( Username nd Password)
	public void login_reesha(String sUserName, String sPassword){
		log.info("login_reesha: " + txtbx_UserName.toString());
		txtbx_UserName.sendKeys(sUserName);
		txtbx_Password.sendKeys(sPassword);
		btn_Login.click();
		log.info("login_reesha: " + btn_Login.getText());
	}
}
