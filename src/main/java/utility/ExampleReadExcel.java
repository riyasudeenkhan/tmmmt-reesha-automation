package utility;

import java.io.File;

import java.io.FileInputStream;

import java.io.FileOutputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.ss.usermodel.Cell;

import org.apache.poi.ss.usermodel.Row;

import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


	public class ExampleReadExcel {
		
		public static void main(String[] args) throws IOException {
		    String excelFilePath = System.getProperty("user.dir")+"\\src\\test\\resources\\TestAutoRest.xlsx";
		    ExampleReadExcel reader = new ExampleReadExcel();
		    List<Book> listBooks = reader.readBooksFromExcelFile(excelFilePath);
		    System.out.println("Size: " + listBooks.size()+ " First title: "+listBooks.get(1).getTitle());
		}

		private Object getCellValue(Cell cell) {
		    switch (cell.getCellType()) {
			    case Cell.CELL_TYPE_STRING:
			        return cell.getStringCellValue();
			 
			    case Cell.CELL_TYPE_BOOLEAN:
			        return cell.getBooleanCellValue();
			 
			    case Cell.CELL_TYPE_NUMERIC:
			        return cell.getNumericCellValue();
		    }
		 
		    return null;
		}
		
		public List<Book> readBooksFromExcelFile(String excelFilePath) throws IOException {
				List<Book> listBooks = new ArrayList<>();
				FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
				Workbook workbook = new XSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(0);
				Iterator<Row> iterator = firstSheet.iterator();
				Row nextRow = iterator.next();
				while (iterator.hasNext()) {
					nextRow = iterator.next();
					Iterator<Cell> cellIterator = nextRow.cellIterator();
					Book aBook = new Book();
					while (cellIterator.hasNext()) {
						Cell nextCell = cellIterator.next();
						int columnIndex = nextCell.getColumnIndex();
						switch (columnIndex) {
							case 1:
								//aBook.setTitle((String) getCellValue(nextCell));
								aBook.setTitle(nextCell.getStringCellValue());
								System.out.println("columnIndex = "+columnIndex+"  value= "+nextCell.getStringCellValue());
								break;
							case 2:
								//aBook.setAuthor((String) getCellValue(nextCell));
								aBook.setAuthor(nextCell.getStringCellValue());
								System.out.println("columnIndex = "+columnIndex);
								break;
							case 3:
								System.out.println("columnIndex = "+columnIndex);
								break;
							case 4:
								System.out.println("columnIndex = "+columnIndex);
								break;
							case 5:
								System.out.println("columnIndex = "+columnIndex);
								//aBook.setPrice((double) getCellValue(nextCell));
								aBook.setPrice(nextCell.getNumericCellValue());
								break;
						}
					}
					listBooks.add(aBook);
				}
				
				workbook.close();
				inputStream.close();
				
				return listBooks;
		}

}


class Book {
	private String title;
	private String author;
	private double price;
	
	public Book() {
	}
	
	/*public String toString() {
		return String.format("%s - %s - %f", title, author, price);
    }*/
	
	// getters and setters
	public void setTitle(String sTitle) {
		this.title = sTitle.toString();
	}
	
	public void setAuthor(String sAuthor) {
		this.author = sAuthor.toString();
	}
	
	public void setPrice(double fPrice) {
		this.price = fPrice;
	}
	
	
	public String getTitle() {
		return this.title;
	}
	
	public String getAuthor() {
		return this.author;
	}
	
	public double getPrice() {
		return this.price;
	}
}




