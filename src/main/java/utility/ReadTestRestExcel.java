package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


	public class ReadTestRestExcel {
		public String RESTNAME = "TestAutoRest";
		
		/*public static void main(String[] args) throws IOException {
		    String excelFilePath = System.getProperty("user.dir")+"\\src\\test\\resources\\TestAutoRest.xlsx";
		    ReadTestRestExcel reader = new ReadTestRestExcel();
		    List<Product> listProds = reader.readProductsFromExcelFile(excelFilePath);
		    System.out.println("Name: " + listProds.size()+ " First Price: "+listProds.get(0).getPrice());
		}*/
		
		public List<Product> readProductsFromExcelFile(String excelFilePath) throws IOException {
				List<Product> listProds = new ArrayList<>();
				FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
				Workbook workbook = new XSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(0);
				Iterator<Row> iterator = firstSheet.iterator();
				Row nextRow = iterator.next();
				while (iterator.hasNext()) {
					nextRow = iterator.next();
					Iterator<Cell> cellIterator = nextRow.cellIterator();
					Product aProd = new Product();
					while (cellIterator.hasNext()) {
						Cell nextCell = cellIterator.next();
						int columnIndex = nextCell.getColumnIndex();
						switch (columnIndex) {
							case 1:
								aProd.setName(nextCell.getStringCellValue());
								break;
							case 2:
								aProd.setArName(nextCell.getStringCellValue());
								break;
							case 3:
								aProd.setDesc(nextCell.getStringCellValue());
								break;
							case 4:
								aProd.setArDesc(nextCell.getStringCellValue());
								break;
							case 5:
								aProd.setPrice(nextCell.getNumericCellValue());
								break;
							case 6:
								aProd.setCat(nextCell.getStringCellValue());
								break;
							case 7:
								aProd.setArCat(nextCell.getStringCellValue());
								break;
						}
					}
					listProds.add(aProd);
				}
				
				workbook.close();
				inputStream.close();
				
				return listProds;
		}

}





