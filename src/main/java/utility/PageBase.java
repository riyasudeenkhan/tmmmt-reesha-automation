package utility;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;


public abstract class PageBase {
	Logger log = Logger.getLogger(PageBase.class);

	protected WebDriver driver;
	public String RESTNAME = "TestAutoRest";
	protected List<Product> listProds;

	@FindBy(how = How.XPATH, using = "//a[@class='m-brand__logo-wrapper']")
	@CacheLookup
	public WebElement img_TmmmtLogo;
	
	public PageBase(WebDriver driver) {
		this.driver = driver;
	}

	public WebDriver getWebDriver() {
		return driver;
	}

	public String getTitle() {
		return driver.getTitle();
	}

	public Boolean hasText(String text) {
		return this.driver.getPageSource().contains(text);
	}
	
	public void implicitWait(int seconds) {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}
	
	public List<Product> readTestExcelMenus() {
		try {
			String excelFilePath = System.getProperty("user.dir")+"\\src\\test\\resources\\TestAutoRest.xlsx";
		    ReadTestRestExcel reader = new ReadTestRestExcel();
		    listProds = reader.readProductsFromExcelFile(excelFilePath);
		} catch(IOException e) {
			e.printStackTrace();
		}
		return listProds;
	}
	
	public void waitForPageLoad(int seconds) {
		seconds = seconds*1000;
		try {
			Thread.sleep(seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isElementPresent(WebElement elem) {
		boolean isPresent = true;
		try {
			isPresent = elem.isDisplayed();
		} catch (NoSuchElementException e) {
			isPresent = false;
		}
		return isPresent;
	}
	
	public WebElement fluientWaitforElement(WebElement element, int timoutSec, int pollingSec) {

	    FluentWait<WebDriver> fWait = new FluentWait<WebDriver>(driver).withTimeout(timoutSec, TimeUnit.SECONDS)
	    .pollingEvery(pollingSec, TimeUnit.SECONDS)
	    .ignoring(NoSuchElementException.class, TimeoutException.class);

	    for (int i = 0; i < 2; i++) {
	        try {
	        	log.debug("fluientWaitforElement i = "+i);
	            fWait.until(ExpectedConditions.visibilityOf(element));
	            fWait.until(ExpectedConditions.elementToBeClickable(element));
	        } 
	        catch (Exception e) {

	        	log.debug("Element Not found trying again ");
	            e.printStackTrace();
	        }
	    }

	    return element;
	}
	
	public void clickOn(WebElement elem, WebDriver driver, int timeout)
	{
		try {
		    final WebDriverWait wait = new WebDriverWait(driver, timeout);
		    wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(elem)));
		    elem.click();
		    waitForPageLoad(5);
		} catch (StaleElementReferenceException e) {
			e.printStackTrace();
		}	
		    
	}
	
	public void redirectURL(String replStr, String appendStr) {
		log.info("Click on TmmmT logo to go home page.");
		img_TmmmtLogo.click();
		waitForPageLoad(5);
		
		log.info("Start redirecting into new page");
		String url = driver.getCurrentUrl();
		url = url.replaceAll(replStr, appendStr);
		log.debug("newurl: "+url);
		driver.get(url);
		waitForPageLoad(10);
	}
	
	public void byVisibleElement(WebElement Element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
     		
		//This will scroll the page till the element is found		
		js.executeScript("arguments[0].scrollIntoView(true);", Element);
		
		//This will scroll the web page till end.		
        //js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		log.debug("End byVisibleElement");
	}
	
}
