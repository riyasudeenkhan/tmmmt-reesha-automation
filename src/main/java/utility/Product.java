package utility;

public class Product {
	private String name;
	private String arName;
	private String desc;
	private String arDesc;
	private double price;
	private String cat;
	private String arCat;
	
	public Product() {
	}
	
	/*public String toString() {
			return String.format("%s - %s - %f", title, author, price);
	    }*/
	
	// getters and setters
	public void setName(String sName) {
		this.name = sName;
	}
	
	public void setArName(String sArName) {
		this.arName = sArName;
	}
	
	public void setDesc(String sDesc) {
		this.desc = sDesc;
	}
	
	public void setArDesc(String sArDesc) {
		this.arDesc = sArDesc;
	}
	
	public void setPrice(double dPrice) {
		this.price = dPrice;
	}
	
	public void setCat(String sCat) {
		this.cat = sCat;
	}
	
	public void setArCat(String sArCat) {
		this.arCat = sArCat;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getArName() {
		return this.arName;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public String getArDesc() {
		return this.arDesc;
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public String getCat() {
		return this.cat;
	}
	
	public String getArCat() {
		return this.arCat;
	}

}
