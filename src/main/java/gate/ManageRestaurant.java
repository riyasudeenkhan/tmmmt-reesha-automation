package gate;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import utility.PageBase;

public class ManageRestaurant extends PageBase{
	Logger log = Logger.getLogger(ManageRestaurant.class);
	
	@FindBy(how = How.CSS, using = "i.m-menu__link-icon.flaticon-tea-cup")
	@CacheLookup
	public WebElement icn_Rest;
	
	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'Add Restaurant')]")
	@CacheLookup
	public WebElement lab_AddRest;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.code']/following-sibling::input")
	@CacheLookup
	public WebElement txt_Code;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Select a category')]")
	@CacheLookup
	public WebElement sel_Category;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Select a category')]/../div[4]/div/input")
	@CacheLookup
	public WebElement txt_CatSearch;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Arabian')]")
	@CacheLookup
	public WebElement val_Categ;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.name']/following-sibling::input")
	@CacheLookup
	public WebElement txt_NameEng;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.arName']/following-sibling::input")
	@CacheLookup
	public WebElement txt_NameArab;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.openAt']/following-sibling::ngb-timepicker//input[@placeholder='HH']")
	@CacheLookup
	public WebElement txt_OpenHH;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.openAt']/following-sibling::ngb-timepicker//input[@placeholder='MM']")
	@CacheLookup
	public WebElement txt_OpenMM;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.closeAt']/following-sibling::ngb-timepicker//input[@placeholder='HH']")
	@CacheLookup
	public WebElement txt_CloseHH;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.closeAt']/following-sibling::ngb-timepicker//input[@placeholder='MM']")
	@CacheLookup
	public WebElement txt_CloseMM;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.closeAt']/following-sibling::ngb-timepicker//div[5]")
	@CacheLookup
	public WebElement btn_CloseAM;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='multipleShift']")
	@CacheLookup
	public WebElement chk_MultiShift;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.openAt2']/following-sibling::ngb-timepicker//input[@placeholder='HH']")
	@CacheLookup
	public WebElement txt_Open2HH;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.openAt2']/following-sibling::ngb-timepicker//input[@placeholder='MM']")
	@CacheLookup
	public WebElement txt_Open2MM;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.closeAt2']/following-sibling::ngb-timepicker//input[@placeholder='HH']")
	@CacheLookup
	public WebElement txt_Close2HH;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.closeAt2']/following-sibling::ngb-timepicker//input[@placeholder='MM']")
	@CacheLookup
	public WebElement txt_Close2MM;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.openAt2']/following-sibling::ngb-timepicker//div[5]")
	@CacheLookup
	public WebElement btn_Open2AM;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='restaurant.closeAt2']/following-sibling::ngb-timepicker//div[5]")
	@CacheLookup
	public WebElement btn_Close2AM;
	
	@FindBy(how = How.ID, using = "autoComplete")
	@CacheLookup
	public WebElement txt_SearchLoc;
	
	@FindBy(how = How.XPATH, using = "//input[@id='autoComplete']")
	@CacheLookup
	public WebElement txt_SearchBox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='pac-container pac-logo']/div[1]")
	public WebElement sel_FirstLoc;

	@FindBy(how = How.XPATH, using = "//textarea[@formcontrolname='address']")
	@CacheLookup
	public WebElement txt_Address;
	
	@FindBy(how = How.XPATH, using = "//textarea[@formcontrolname='addressNote']")
	@CacheLookup
	public WebElement txt_AddrNote;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.picture']/following-sibling::div//input")
	@CacheLookup
	public WebElement img_Picture;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.logo']/following-sibling::div//input")
	@CacheLookup
	public WebElement img_Logo;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='deliveryFree']")
	@CacheLookup
	public WebElement txt_FreeDlv;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Submit')]")
	//@CacheLookup
	public WebElement btn_Submit;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(), 'Update Restaurant')]")
	@CacheLookup
	public WebElement btn_UpdRest;
	
	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'Update Restaurant')]")
	@CacheLookup
	public WebElement lab_UpdRest;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.picture']/following-sibling::div//input")
	@CacheLookup
	public WebElement img_Pic;
	
	public ManageRestaurant(WebDriver driver)
	{
		super(driver);
	}
	
	//{"Email", "pass", "code", "category", "name", "ar-name", "ohh", "omm", "chh", "cmm", "addNote", "img-path", "free-d"};
	//String[] inputs = {"herfy@tmmmt.com", "tmmmt123", "11111112042017", "Arabian", "Herfy", "herfy", "09", "00", "11", "00", "Subway", "C:\\Users\\SSC\\Documents\\herfy.jpg", "50"};
	public Boolean addRestaurant(String code, String category, String name, String ar_name, String ohh, String omm, String chh, String cmm, String address, String logo) {
		implicitWait(20);
		log.info("addRestaurant: Start Adding Restaurant " + name);
		//driver.findElement(By.cssSelector("i.m-menu__link-icon.flaticon-tea-cup")).click();
		icn_Rest.click();
		implicitWait(20);
		if(!lab_AddRest.isDisplayed()) {
			log.error("Add Restaurant is not available");
			return false;
		}
		txt_Code.clear();txt_Code.sendKeys(code);
		sel_Category.click();
		txt_CatSearch.clear();txt_CatSearch.sendKeys(category);
		val_Categ.click();
		txt_NameEng.clear();txt_NameEng.sendKeys(name);
		txt_NameArab.clear();txt_NameArab.sendKeys(ar_name);
		txt_OpenHH.clear();txt_OpenHH.sendKeys(ohh);
		txt_OpenMM.clear();txt_OpenMM.sendKeys(omm);
		txt_CloseHH.clear();txt_CloseHH.sendKeys(chh);
		txt_CloseMM.clear();txt_CloseMM.sendKeys(cmm);
		btn_CloseAM.click();
		/*txt_SearchLoc.sendKeys(searchLoc);
		implicitWait(10);
		//txt_SearchLoc.click();
		//implicitWait(10);
		if(!sel_FirstLoc.isDisplayed()) {
			log.info("Locations are not available.");
			txt_SearchBox.click();
			implicitWait(10);
		}
		sel_FirstLoc.click();*/
		
		txt_Address.sendKeys(address);
		img_Logo.sendKeys(logo);
		waitForPageLoad(5);
		btn_Submit.click();
		
		log.info("addRestaurant: End Adding Restaurant "+ name);
		return true;
	}
	
	public Boolean updateRestaurant(String picPath) {
		log.info("updateRestaurant: Started");
		icn_Rest.click();
		implicitWait(20);
		btn_UpdRest.click();
		implicitWait(20);
		if(!lab_UpdRest.isDisplayed()) {
			log.error("Update Restaurant is not available");
			return false;
		}
		img_Pic.sendKeys(picPath);
		waitForPageLoad(5);
		log.debug("Update Submission " + btn_Submit.getTagName());
		btn_Submit.click();
		
		log.info("updateRestaurant: End");
		return true;
	}
}
