package gate;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import utility.PageBase;

public class ManageRestMenus extends PageBase{
	Logger log = Logger.getLogger(ManageRestMenus.class);
	
	@FindBy(how = How.CSS, using = "i.m-menu__link-icon.flaticon-list-1")
	@CacheLookup
	public WebElement icn_Menu;
	
	//@FindBy(how = How.XPATH, using = "//app-primeng-button/div[2]//button/i")
	@FindBy(how = How.CSS, using = "i.la.la-cart-plus")
	public WebElement btn_AddMenu;
	
	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'Add Menu')]")
	public WebElement lab_AddMenu;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.name']/following-sibling::input")
	public WebElement txt_Name;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.arName']/following-sibling::input")
	public WebElement txt_arName;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.category']/following-sibling::p-dropdown/div/div[3]")
	public WebElement btn_Category;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.category']/following-sibling::p-dropdown/div/div[4]//input")
	public WebElement filter_Cat;
	
	@FindBy(how = How.XPATH, using = "//li[contains(text(),'No results found')]")
	public WebElement noCatFount;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.category']/following-sibling::p-dropdown/div/div[4]//ul")
	public WebElement sel_Category;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.category']/following-sibling::p-dropdown/div/input")
	public WebElement txt_Category;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.arCategory']/following-sibling::p-dropdown/div/div[3]")
	public WebElement btn_arCategory;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.arCategory']/following-sibling::p-dropdown/div/div[4]//input")
	public WebElement filter_arCat;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.arCategory']/following-sibling::p-dropdown/div/div[4]//ul")
	public WebElement sel_arCategory;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.arCategory']/following-sibling::p-dropdown/div/input")
	public WebElement txt_ArCategory;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='isTimeBased']")
	public WebElement chk_isTimeBased;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(), 'From')]/following-sibling::ngb-timepicker//input[@placeholder='HH']")
	public WebElement txt_FromHH;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(), 'From')]/following-sibling::ngb-timepicker//input[@placeholder='MM']")
	public WebElement txt_FromMM;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(), 'From')]/following-sibling::ngb-timepicker//button[contains(text(),'AM')]")
	public WebElement txt_FromAM;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(), 'To')]/following-sibling::ngb-timepicker//input[@placeholder='HH']")
	public WebElement txt_ToHH;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(), 'To')]/following-sibling::ngb-timepicker//input[@placeholder='MM']")
	public WebElement txt_ToMM;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(), 'To')]/following-sibling::ngb-timepicker//button[contains(text(),'AM')]")
	public WebElement txt_ToAM;
	
	@FindBy(how = How.XPATH, using = "//textarea[@formcontrolname='prodDescription']")
	public WebElement txt_Description;
	
	@FindBy(how = How.XPATH, using = "//textarea[@formcontrolname='arProdDescription']")
	public WebElement txt_arDescription;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='cost']")
	public WebElement txt_Cost;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(), 'Cash')]/preceding-sibling::div")
	@CacheLookup
	public WebElement rad_Cash;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(), 'Wallet')]/preceding-sibling::div")
	@CacheLookup
	public WebElement rad_Wallet;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(), 'Any')]/preceding-sibling::div")
	@CacheLookup
	public WebElement rad_Any;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.picture']/following-sibling::input")
	public WebElement file_Picture;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='menu.options']/following-sibling::button")
	@CacheLookup
	public WebElement btn_AddOption;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='menu.options']/following-sibling::div/div/div[1]//button")
	@CacheLookup
	public WebElement btn_CloseOption;
	
	@FindBy(how = How.XPATH, using="//input[@formcontrolname='option']")
	public List<WebElement> txt_options;
	
	@FindBy(how = How.XPATH, using="//input[@formcontrolname='arOption']")
	public List<WebElement> txt_arOptions;
	
	@FindBy(how = How.XPATH, using="//input[@formcontrolname='isMulti']/following-sibling::span")
	public List<WebElement> chk_isMultis;
	
	@FindBy(how = How.XPATH, using="//input[@formcontrolname='isMandatory']/following-sibling::span")
	public List<WebElement> chk_isMandats;
	
	@FindBy(how = How.XPATH, using="//label[@translate='menu.values']/following-sibling::button")
	public List<WebElement> btn_values;
	
	@FindBy(how = How.XPATH, using="//input[@formcontrolname='value']")
	public List<WebElement> txt_values;
	
	@FindBy(how = How.XPATH, using="//input[@formcontrolname='arValue']")
	public List<WebElement> txt_arValues;
	
	@FindBy(how = How.XPATH, using="//input[@formcontrolname='price']")
	public List<WebElement> txt_prices;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Submit')]")
	public WebElement btn_Submit;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'TestMenu')]/../following-sibling::div/div/i[@title='Edit']")
	@CacheLookup
	public WebElement btn_Edit;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'TestMenu')]/../following-sibling::div/div/i[4]")
	@CacheLookup
	public WebElement btn_Delete;
	
	@FindBy(how = How.XPATH, using = "//i[@title='Meals']")
	@CacheLookup
	public WebElement btn_Meals;
	
	@FindBy(how = How.XPATH, using = "//i[@title='Sync']")
	@CacheLookup
	public WebElement btn_Sync;
	
	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'Update Menu')]")
	@CacheLookup
	public WebElement lab_UpdateMenu;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Yes, delete it!')]")
	@CacheLookup
	public WebElement btn_DelConfirm;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'No, cancel!')]")
	@CacheLookup
	public WebElement btn_DelCancel;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'OK')]")
	@CacheLookup
	public WebElement btn_OK;
	
	@FindBy(how = How.XPATH, using = "//h3[@translate='sideMenu.master.menu']")
	@CacheLookup
	public WebElement lab_Menu;
	
	public ManageRestMenus(WebDriver driver) {
		super(driver);
		readTestExcelMenus();
	}
	
	public Boolean addMenu(String name, String arName, String category, String arCategory, String desc, String arDesc, String price, String picLoc) {
		log.info("addMenu, Start add the product");
		//System.out.println("Name: " + listProds.size()+ " First Price: "+listProds.get(0).getPrice());
		Boolean runOnce = true;
		log.debug("addMenu, Goto Menu page and Click Add Menu");
		gotoMenuPage();
		
		if(!isElementPresent(btn_AddMenu)) {
			log.error(btn_AddMenu.getText() + " Element not present");
			return false;
		}
		
		for(int i=0; i<4; i++) {
			name = listProds.get(i).getName();
			arName = listProds.get(i).getArName();
			category = listProds.get(i).getCat();
			arCategory = listProds.get(i).getArCat();
			desc = listProds.get(i).getDesc();
			arDesc = listProds.get(i).getArDesc();
			price = String.valueOf(listProds.get(i).getPrice());
			
			log.info("ClickOn Add Menu button!");
			//clickOn(btn_AddMenu, driver, 20);
			waitForPageLoad(10);
			btn_AddMenu.click();
			waitForPageLoad(5);
			if(!lab_AddMenu.isDisplayed()) {
				log.error("Add Menu page is not available");
				return false;
			}
			log.debug("addMenu, Start input product details");
			txt_Name.clear();txt_Name.sendKeys(name);
			txt_arName.clear();txt_arName.sendKeys(arName);
			
			btn_Category.click();
			filter_Cat.clear();filter_Cat.sendKeys(category);
			if(isElementPresent(noCatFount)) {
				txt_Category.click();
				txt_Category.sendKeys(category);
			}
			else {
				sel_Category.click();
			}			
			
			
			btn_arCategory.click();
			filter_arCat.clear();filter_arCat.sendKeys(arCategory);
			if(isElementPresent(noCatFount)) {
				txt_ArCategory.click();
				txt_ArCategory.sendKeys(arCategory);
			}
			else {
				sel_arCategory.click();
			}
			
			txt_Description.clear();txt_Description.sendKeys(desc);
			txt_arDescription.clear();txt_arDescription.sendKeys(arDesc);
			txt_Cost.clear();txt_Cost.sendKeys(price);
			file_Picture.sendKeys(picLoc);
			
			String[] options = {"Select the options", "Choose one", "Numbers"};
			String[] arOptions = {"Select the options", "Choose one", "Numbers"}; 
			String[][] values = {{"abc", "def"}, {"abc", "def"}, {"123", "456"}};
			String[][] arValues = {{"ghi", "jkl"}, {"ghi", "jkl"}, {"123", "456"}}; 
			String[][] prices = {{"0", "0"}, {"0", "0"}, {"10", "20"}};
			if(options.length > 0 && runOnce) {
				log.info("addMenu, add options if any.");
				addOptions(options, arOptions, values, arValues, prices);
				runOnce = false;
				log.info("addMenu, Options are added.");
			}
			clickOn(btn_Submit, driver, 10);
		}
		log.info("addMenu, End add menu");
		
		return true;
	}
	
	public Boolean updateMenu() {
		log.info("updateMenu, Start edit the product");
		gotoMenuPage();	
		log.debug("updateMenu, Edit Menu button");
		waitForPageLoad(5);
		if(!isElementPresent(btn_Edit)) {
			log.error(btn_Edit.getText() + " Element not present");
			return false;
		}
		clickOn(btn_Edit, driver, 20);
		if(!lab_UpdateMenu.isDisplayed()) {
			log.error("Update Menu page is not available");
			return false;
		}
		rad_Cash.click();
		waitForPageLoad(10);		
		//clickOn(btn_Submit, driver, 10);
		btn_Submit.click();
		log.info("updateMenu, Editing product done.");
		return true;
	}
	
	public void deleteMenu() {
		log.info("deleteMenu, Start deleting product TestMenu");
		gotoMenuPage();	
		waitForPageLoad(5);
		btn_Delete.click();
		implicitWait(5);
		log.debug("deleteMenu, Confirm Delete?");
		btn_DelConfirm.click();
		implicitWait(5);
		btn_OK.click();
		log.info("deleteMenu, Successfully deleted product TestMenu");
	}
	
	private void gotoMenuPage() {
		log.debug("gotoMenuPage, Goto Menu Page");
		implicitWait(20);
		if(isElementPresent(lab_Menu)) {
			log.info("gotoMenuPage, Already in the same page.");
			return;
		}
		fluientWaitforElement(icn_Menu, 10, 5).click();
		//implicitWait(20);
	}
	
	private void addOptions(String[] options, String[] arOptions, String[][] values, String[][] arValues, String[][] prices) {
		log.info("addOptions, Start adding Options");
		
		int c = 0, d = 0; //counter
		int optSz = options.length;
		log.info("updateMenu, no of options available" + optSz);
		Boolean addVal = true;
		for (int i = 0; i < optSz; i++) {
			if(addVal) {
				btn_AddOption.click();
				waitForPageLoad(5);
				//addVal = false;
			}
			txt_options.get(i).clear();txt_options.get(i).sendKeys(options[i]);
			txt_arOptions.get(i).clear();txt_arOptions.get(i).sendKeys(arOptions[i]);
			chk_isMultis.get(i).click();
			log.debug("txt_values: size: "+txt_values.size());
			waitForPageLoad(5);
			int valSz = values[i].length; c = valSz;
			log.debug("Value element size: "+valSz);
			for (int j = 0; j < valSz; j++) {
				if(addVal) {
					while(c-1 > 0) {
						btn_values.get(i).click();
						waitForPageLoad(5);
						c--;
					}
					c = 0;
				}
				txt_values.get(j+d).clear();txt_values.get(j+d).sendKeys(values[i][j]);
				log.debug("element "+(j+d)+": "+txt_values.get(j+d).toString());
				txt_arValues.get(j+d).clear();txt_arValues.get(j+d).sendKeys(arValues[i][j]);
				txt_prices.get(j+d).clear();txt_prices.get(j+d).sendKeys(prices[i][j]);
				addVal = false;
				c = j+1;
			}
			d = d + c;
			addVal = true;
		}
		
		log.info("addOptions, Successfully added Options");
	}
}
