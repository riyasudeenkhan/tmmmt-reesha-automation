package gate;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utility.PageBase;

public class Gate_Login extends PageBase{
	Logger log = Logger.getLogger(Gate_Login.class);
	
	@FindBy(how = How.NAME, using = "email")
	@CacheLookup
	public WebElement txtbx_UserName;
 
	@FindBy(how = How.NAME, using = "password")
	@CacheLookup
	public WebElement txtbx_Password;
 
	@FindBy(how = How.ID, using = "m_login_signin_submit")
	@CacheLookup
	public WebElement btn_Login ;
	
	public Gate_Login(WebDriver driver)
	{
		super(driver);
	}
 
	// This method will take two arguments ( Username nd Password)
	public void login_gate(String sUserName, String sPassword){
		txtbx_UserName.sendKeys(sUserName);
		log.info("login_gate: Enter Usernam");
		txtbx_Password.sendKeys(sPassword);
		log.info("login_gate: Enter Password");
		btn_Login.click();
		log.info("login_gate: " + btn_Login.getText());
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
}
