package gate;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import utility.PageBase;

public class ManageBranches extends PageBase{
	Logger log = Logger.getLogger(ManageBranches.class);
	
	@FindBy(how = How.CSS, using = "i.m-menu__link-icon.flaticon-map")
	@CacheLookup
	public WebElement icn_Branch;
	
	@FindBy(how = How.XPATH, using = "//div[@class='mCSB_container']/ul/li[11]")
	public WebElement div_SideMenuFrame;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create Branch')]")
	@CacheLookup
	public WebElement btn_CreateBranch;
	
	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'Create Branch')]")
	@CacheLookup
	public WebElement lab_Create;
	
	@FindBy(how = How.XPATH, using = " //input[@formcontrolname='username']")
	@CacheLookup
	public WebElement txt_Username;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='name']")
	@CacheLookup
	public WebElement txt_Name;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='email']")
	@CacheLookup
	public WebElement txt_Email;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='iban']")
	@CacheLookup
	public WebElement txt_Iban;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'City')]/following-sibling::p-autocomplete//button")
	@CacheLookup
	public WebElement dd_City;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'City')]/following-sibling::p-autocomplete//input")
	@CacheLookup
	public WebElement txt_CityName;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'City')]/following-sibling::p-autocomplete//ul/li[1]")
	@CacheLookup
	public WebElement sel_City;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='mobile']")
	@CacheLookup
	public WebElement txt_Mobile;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='password']")
	@CacheLookup
	public WebElement txt_Password;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='rpassword']")
	@CacheLookup
	public WebElement txt_rPassword;
	
	@FindBy(how = How.XPATH, using = "//label[@translate='common.document']/following-sibling::input")
	@CacheLookup
	public WebElement file_Path;
	
	@FindBy(how = How.NAME, using = "branchRestaurantCode")
	@CacheLookup
	public WebElement txt_RestCode;
	
	@FindBy(how = How.NAME, using = "resname")
	@CacheLookup
	public WebElement txt_RestName;
	
	@FindBy(how = How.XPATH, using = "//textarea[@formcontrolname='address']")
	@CacheLookup
	public WebElement txt_Address;
	
	@FindBy(how = How.XPATH, using = "//textarea[@formcontrolname='addressNote']")
	@CacheLookup
	public WebElement txt_AddrNote;
	
	@FindBy(how = How.XPATH, using = "//input[@formcontrolname='cloneMenus']/following-sibling::span")
	@CacheLookup
	public WebElement chk_isClone;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Submit')]")
	@CacheLookup
	public WebElement btn_Submit;
	
	@FindBy(how = How.ID, using = "editButton")
	//@CacheLookup
	public WebElement btn_Edit;
	
	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'Update Branch')]")
	@CacheLookup
	public WebElement lab_Edit;
	
	
	public ManageBranches(WebDriver driver)
	{
		super(driver);
	}
	
	public Boolean createBranch(String branchName) {
		waitForPageLoad(10);
		log.info("createBranch: Start Creating Branch " + branchName);
		
		log.info("RedirectURL into Branch Page.");
		//byVisibleElement(div_SideMenuFrame);
		redirectURL("index", "branches");
		
//		icn_Branch.click();		//commented due to not able to click the element since achieving scroll down function failed
//		waitForPageLoad(5);
		
		if(!isElementPresent(btn_CreateBranch)) {
			log.error(btn_CreateBranch.getText() + " Element not present");
			return false;
		}
		log.info("ClickOn Create Branch button!");
		//clickOn(btn_AddMenu, driver, 20);
		btn_CreateBranch.click();
		implicitWait(20);
		if(!lab_Create.isDisplayed()) {
			log.error("Create Branch page is not available");
			return false;
		}
		
		log.debug("createBranch, Start input product details");
		txt_Username.clear();txt_Username.sendKeys("TestBranch2");
		txt_Name.clear();txt_Name.sendKeys("Test Branch 2");
		txt_Email.clear();txt_Email.sendKeys("testbranch2@tmmmt.com");
		txt_Iban.clear();txt_Iban.sendKeys("12345");
		dd_City.click();
		implicitWait(5);
		txt_CityName.sendKeys("Chennai");
		implicitWait(5);
		sel_City.click();
		txt_Mobile.clear();txt_Mobile.sendKeys("8200150318");
		txt_Password.clear();txt_Password.sendKeys("tmmmt123");
		txt_rPassword.clear();txt_rPassword.sendKeys("tmmmt123");
		//file_Path.sendKeys("");
		txt_RestCode.sendKeys("1234567000");
		txt_RestName.clear();txt_RestName.sendKeys(branchName);
		txt_Address.clear();txt_Address.sendKeys("Alandur, Chennai");
		//txt_AddrNote.clear();txt_AddrNote.sendKeys("");
		chk_isClone.click();
		waitForPageLoad(5);
		btn_Submit.click();
		
		log.info("createBranch: End Creating Branch "+ branchName);
		return true;
	}
	
	public Boolean updateBranch(String branchName) {
		waitForPageLoad(10);
		log.info("updateBranch: Start Updating Branch " + branchName);
		log.info("RedirectURL into Branch Page.");
		redirectURL("index", "branches");
		
		if(!isElementPresent(btn_Edit)) {
			log.error(btn_Edit.getText() + " Element not present");
			return false;
		}
		log.info("ClickOn Edit Branch button!");
		btn_Edit.click();
		implicitWait(20);
		if(!lab_Edit.isDisplayed()) {
			log.error("Edit Branch page is not available");
			return false;
		}
		
		txt_Address.clear();txt_Address.sendKeys("Alandur, Chennai, Tamil Nadu, India");
		waitForPageLoad(5);
		btn_Submit.click();
		
		log.debug("updateBranch, Start input product details");
		
		return true;
	}
}
