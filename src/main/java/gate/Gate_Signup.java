package gate;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import utility.PageBase;

public class Gate_Signup extends PageBase{
	Logger log = Logger.getLogger(Gate_Signup.class);
	
	@FindBy(how = How.ID, using = "m_login_signup")
	@CacheLookup
	public WebElement btn_Signup;
 
	@FindBy(how = How.NAME, using = "username")
	@CacheLookup
	public WebElement txtbx_Username;
	
	//@FindBy(how = How.NAME, using = "email")
	@FindBy(how = How.XPATH, using = "//input[@name='username']/../following-sibling::div[1]/input")
	@CacheLookup
	public WebElement txtbx_Email;
	
	@FindBy(how = How.NAME, using = "iban")
	@CacheLookup
	public WebElement txtbx_Iban;
	
	@FindBy(how = How.XPATH, using = "//p-dropdown[@name='countryCode']//label")
	public WebElement sel_Code ;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'+91')]")
	public WebElement sel_CountryCode ;
	
	@FindBy(how = How.NAME, using = "mobile")
	@CacheLookup
	public WebElement txtbx_Mobile;
	
	//@FindBy(how = How.NAME, using = "password")
	@FindBy(how = How.XPATH, using = "//input[@name='mobile']/../following-sibling::div[1]/input")
	@CacheLookup
	public WebElement txtbx_Password;
	
	@FindBy(how = How.NAME, using = "rpassword")
	@CacheLookup
	public WebElement txtbx_RPassword;
	
	@FindBy(how = How.NAME, using = "imgFile")
	@CacheLookup
	public WebElement txtbx_FilePath;
 
	@FindBy(how = How.XPATH, using = "//input[@name='agree']/following-sibling::span")
	@CacheLookup
	public WebElement chk_Agree;
	
	@FindBy(how = How.ID, using = "m_login_signup_submit")
	@CacheLookup
	public WebElement btn_Submit;
	
	@FindBy(how = How.ID, using = "m_login_signup_cancel")
	@CacheLookup
	public WebElement btn_Cancel;
 
	public Gate_Signup(WebDriver driver)
	{
		super(driver);
	}
 
	// This method will take two arguments ( Username nd Password)
 
	public void signup_gate(String sUserName, String sEmail, String sIban, String sMobile, String sPassword, String sRPassword, String sFilePath){
		log.info("signup_gate: Start" + btn_Signup.getText());
		btn_Signup.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		txtbx_Username.sendKeys(sUserName);
		txtbx_Email.sendKeys(sEmail);
		txtbx_Iban.sendKeys(sIban);
		sel_Code.click();
		sel_CountryCode.click();
		txtbx_Mobile.sendKeys(sMobile);
		txtbx_Password.sendKeys(sPassword);
		txtbx_RPassword.sendKeys(sRPassword);
		txtbx_FilePath.sendKeys(sFilePath);
		//chk_Agree.click();	// Removed this element from Signup page. 11/04/2018
		btn_Submit.submit();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		log.info("signup_gate: End" + btn_Submit.getText()); 
	}
	
	public void signup_cancel() {
		log.info("signup_gate: Start" + btn_Signup.getText());
		btn_Signup.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		btn_Cancel.click();
		log.info("signup_gate: End" + btn_Cancel.getText());
	}
}
