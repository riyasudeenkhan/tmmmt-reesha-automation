package utility;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.codehaus.plexus.util.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

public class ListenerTest implements ITestListener, ISuiteListener {
	Logger log = Logger.getLogger(ListenerTest.class);

	PropertiesConfiguration config;
	private static final String DRIVER = "driver";

	public void onFinish(ITestContext Context) {
		log.info("Test '" + Context.getName() + "' Finished");
		stopDriver(Context);
	}

	public void onStart(ITestContext Context) {
		log.info("Test '" + Context.getName() + "' Started");
		Context.setAttribute(DRIVER, createWebDriver());
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult Result) {
		// TODO Auto-generated method stub

	}

	public void onTestFailure(ITestResult Result) {
		log.info("Test '" + Result.getName() + "' FAILED");
		log.debug("Priority of this method is " + Result.getMethod().getPriority());
		takeScreenShot();
	}

	public void onTestSkipped(ITestResult Result) {
		log.info("Test '" + Result.getName() + "' SKIPPED");

	}

	public void onTestStart(ITestResult Result) {
		log.info(Result.getName() + " test case started");

	}

	public void onTestSuccess(ITestResult Result) {
		log.info("Test '" + Result.getName() + "' PASSED");
		// This will print the class name in which the method is present
		log.info(Result.getTestClass());
	}


	/**
	 * @return - A valid {@link WebDriver} instance only when invoked from within a
	 *         <code>@Test</code> annotated test method.
	 */
	@SuppressWarnings("unchecked")
	public static WebDriver getDriver() {
		ITestResult result = Reporter.getCurrentTestResult();
		if (result == null) {
			throw new UnsupportedOperationException("Please invoke only from within an @Test method");
		}

		Object driver = result.getTestContext().getAttribute(DRIVER);
		if (driver == null) {
			throw new IllegalStateException("Unable to find a valid WebDriver instance");
		}
		if (!(driver instanceof WebDriver)) {
			throw new IllegalStateException("Corrupted WebDriver.");
		}
		return (WebDriver) driver;
	}

	@SuppressWarnings("unchecked")
	public void stopDriver(ITestContext Context) {
		log.debug("Test '" + Context.getName() + "' Finished");
		Object driver = Context.getAttribute(DRIVER);
		if (driver == null) {
			return;
		}
		if (!(driver instanceof WebDriver)) {
			throw new IllegalStateException("Corrupted WebDriver.");
		}

		log.info("Stop driver");
		((WebDriver) driver).quit();

		Context.setAttribute(DRIVER, null);
	}

	public WebDriver createWebDriver() {
		log.info("Create instance for WebDriver");
		System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\chromedriver.exe");
		String env = "";
		String url = "https://abd.tmmmt.com";
		WebDriver driver = null;

		env = config.getString("environment");
		if (env.equalsIgnoreCase("Live")) {
			url = "https://reesha.tmmmt.com";
		} 

		log.debug(env + " ==> " + url);
		log.debug("Start setup selenium");

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		log.info("Open reesha.");
		driver.get(url);

		log.info("WebDriver object created successfully!");

		return driver;
	}

	public void takeScreenShot() {
		String destDir;
		DateFormat dateFormat;
		// Set folder name to store screenshots.
		destDir = "screenshots";
		// Capture screenshot.
		log.info("Take Screenshot");
		WebDriver driver = getDriver();
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// Set date format to set It as screenshot file name.
		dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
		// Create folder under project with name "screenshots" provided to destDir.
		new File(destDir).mkdirs();
		// Set file name using current date time.
		String destFile = dateFormat.format(new Date()) + ".png";
		log.debug("Screenshot name: " + destFile);

		try {
			// Copy paste file at destination folder location
			FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
			log.info("Successfully captured screenshot");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart(ISuite suite) {
		log.debug("Started running suite, "+suite.getName());
		Map<String, String> parameters = suite.getXmlSuite().getParameters();
        for (Map.Entry<String, String> parameter : parameters.entrySet()) {
            String env = System.getenv(parameter.getKey());
            if (env != null && ! env.trim().isEmpty()) {
                parameter.setValue(env);
            }
        }
        
		try
		{
			config = new PropertiesConfiguration("src/main/resources/config.properties");
			String env = suite.getParameter("environment");
			config.setProperty("environment", env);
			/*
			 * String restName = suite.getParameter("restaurantName");
			 * config.setProperty("restaurantName", restName);
			 */
			config.save();
			log.info("successfully updated property file");
		}
		catch (ConfigurationException cex)
		{
		    log.debug("Something went wrong \n" + cex.getMessages());
		}

	}

	@Override
	public void onFinish(ISuite suite) {
		log.debug("Finished on running suite, "+suite.getName());

	}
}
