package utility;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public abstract class TestBase {
	protected static WebDriver driver;
	Logger log = Logger.getLogger(TestBase.class);
	 
	@BeforeClass
	public void init() {
		log.info("TestBase : Start setup Selenium");
		/*
		 * System.setProperty("webdriver.chrome.driver",
		 * ".\\src\\test\\resources\\chromedriver.exe"); driver = new ChromeDriver();
		 * driver.manage().window().maximize();
		 */
		
		driver = ListenerTest.getDriver();
	}
	
	@AfterClass
	public void cleanUp() {
		log.info("Successfully Completed TmmmT Automation!!!");
		//driver.quit();
	}
	
	public void implicitWait(int seconds) {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}
	
	public void waitTime(int seconds) {
		seconds = seconds*1000;
		try {
			Thread.sleep(seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	public Boolean isElementPresent(WebElement el) {
		try {
			return el.isDisplayed();
		} catch (NoSuchElementException ignore) {
			return false;
		} catch (Exception ignore) {
			return false;
		}
	}
}
