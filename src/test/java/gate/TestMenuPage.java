package gate;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utility.TestBase;

public class TestMenuPage extends TestBase{
	ManageRestMenus menu;
	Logger log = Logger.getLogger(TestMenuPage.class);
	
	@Override
	public void init() {
		log.info("Initialize Restaurant Menu Page");
	}
	
	@BeforeClass
	public void setup() {
		menu = PageFactory.initElements(driver, ManageRestMenus.class);
	}
 
	@Test
	public void testAddMenu() {
		log.info("Starts testAddMenu");
		Boolean result = menu.addMenu("TestMenu1", "ArTestMenu1", "TestCategory", "ArTestCategory", "TestDescription", "ArTestDescription", "10", "I:\\TmmmT Automation\\TmmmT-Portal-Automation\\TmmmTAutomation\\src\\test\\resources\\herfy.jpg");
		if(!result) {
			log.error("Add Menu Page is not available.");
			Assert.assertTrue(false);
		}
		log.info(" Successfully Added Product ");
	}
	
	@Test (dependsOnMethods = { "testAddMenu" })
	public void testUpdateMenu() {
		log.info("Starts testUpdateMenu");
		
		Boolean result = menu.updateMenu();
		if(!result) {
			log.error("Update Menu Page is not available.");
			Assert.assertTrue(false);
		}
		log.info(" Successfully Updated Product ");
	}
	
	@Test (dependsOnMethods = { "testAddMenu" })
	public void testDeleteMenu() {
		log.info("testDeleteMenu, Started");
		menu.deleteMenu();
	}
}
