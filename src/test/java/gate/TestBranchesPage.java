package gate;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utility.TestBase;

public class TestBranchesPage extends TestBase{
	ManageBranches branch;
	Logger log = Logger.getLogger(TestBranchesPage.class);
	
	@Override
	public void init() {
		log.info("Initialize Restaurant Menu Page");
	}
	
	@BeforeClass
	public void setup() {
		branch = PageFactory.initElements(driver, ManageBranches.class);
	}
 
	@Test
	public void testCreateBranches() {
		log.info("Starts testCreateBranches");
		Boolean result = branch.createBranch("TestBranch3");
		if(!result) {
			log.error("Create Branches Page is not available.");
			Assert.assertTrue(false);
		}
		log.info(" Successfully Created Branches");
	}
	
	@Test
	public void testUpdateBranches() {
		log.info("Starts testUpdateBranches");
		Boolean result = branch.updateBranch("TestBranch3");
		if(!result) {
			log.error("Update Branches Page is not available.");
			Assert.assertTrue(false);
		}
		log.info(" Successfully Updated Branches");
	}
	
}
