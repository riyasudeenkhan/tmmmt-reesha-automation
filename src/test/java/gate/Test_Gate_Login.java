package gate;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import reesha.Reesha_Login;
import reesha.RestaurantAdmin;
import utility.TestBase;

public class Test_Gate_Login extends TestBase {
	Gate_Login loginPage;
	Gate_Signup signupPage;
	RestaurantAdmin restAdm;
	Reesha_Login reeshLogin;
	Logger log = Logger.getLogger(Test_Gate_Login.class);
 
	@BeforeClass
	public void classSetup() {
		driver.get("https://www.test.tmmmt.com");
		loginPage = PageFactory.initElements(driver, Gate_Login.class);
		signupPage = PageFactory.initElements(driver, Gate_Signup.class);
	}
 
	@Test(priority=3)
	public void testLogin() {
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		loginPage.login_gate("herfy2@tmmmt.com", "tmmmt123");
		log.info(" Login Successfully, now it is the time to Log Off buddy.");
	}
  
	@Test(priority=1)
	public void testSignup() {
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		signupPage.signup_gate("herfy2", "herfy2@tmmmt.com", "herfy1123", "820012003", "tmmmt123", "tmmmt123", System.getProperty("user.dir")+"\\src\\test\\resources\\herfy.jpg");
		log.info(" Restaurant Admin Account Successfully Created");
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		//verify isCreated
		driver.findElement(By.xpath("//span[contains(text(),'Thank you.')]")).getText();
	}
	
	
	public void openReesha() {
		driver.get("https://abd.tmmmt.com");
		implicitWait(20);
		reeshLogin = PageFactory.initElements(driver, Reesha_Login.class);
		reeshLogin.login_reesha("riyas@tmmmt.sa", "tmmmt123");
		implicitWait(30);
		log.info("Login Successfully, now it is the time to Log Off buddy.");
	}
	
	@Test(priority=2)
	public void testActivateRestAdmin() {
		log.info("Starts testActivateRestAdmin");
		openReesha();
		restAdm = PageFactory.initElements(driver, RestaurantAdmin.class);
		restAdm.navigateToRestAdmin();
		restAdm.openRestAdminDetails("820012003");
		//Boolean isActive = restAdm.activateRestAdmin();
		Boolean isActive = true;
		closeReesha();
		if(!isActive) {
			log.error("Restaurant Activation has been failed.");
			Assert.assertTrue(false);
		}
		log.info(" Successfully Activated Restaurant Admin");
	}
	
	public void closeReesha() {
		log.info("Close Reesha Page.");
		driver.get("https://test.tmmmt.com");
		implicitWait(20);
		//driver.close();
	}
	
  
	@Test
	public void testCancel() {
		signupPage.signup_cancel();
		log.info("Successfully working cancel button.");
	}
 
	@Override
	public void cleanUp() {
		log.info("Successfully Completed Automation!!!");
		//driver.quit();
	}
}
