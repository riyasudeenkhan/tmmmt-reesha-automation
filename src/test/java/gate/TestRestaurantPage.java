package gate;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utility.TestBase;

public class TestRestaurantPage extends TestBase{
	ManageRestaurant rest;
	Logger log = Logger.getLogger(TestRestaurantPage.class);
	
	@Override
	public void init() {
		//driver.get("https://abd.tmmmt.com");
		log.info("Initialize RestaurantAdmin");
	}
	
	@BeforeClass
	public void setup() {
		rest = PageFactory.initElements(driver, ManageRestaurant.class);
	}
 
	@Test
	public void testAddRestaurant() {
		log.info("Starts testAddRestaurant");
		log.info("current directory? "+System.getProperty("user.dir"));
		Boolean result = rest.addRestaurant("11111104112018", "Arabian", "Herfy2", "herfy2", "09", "00", "11", "00", "Alandur, Chennai, Tamil Nadu, India", System.getProperty("user.dir")+"\\src\\test\\resources\\herfy.jpg");
		if(!result) {
			log.error("Add Restaurant page is not available.");
			Assert.assertTrue(false);
		}
		log.info(" Successfully Added Restaurant ");
	}
	
	@Test
	public void testUpdateRestaurant() {
		log.info("Starts testUpdateRestaurant");
		Boolean result = rest.updateRestaurant(System.getProperty("user.dir")+"\\src\\test\\resources\\herfy.jpg");
		if(!result) {
			log.error("Update Restaurant page is not available.");
			Assert.assertTrue(false);
		}
		log.info(" Successfully Updated Restaurant ");
	}
}
