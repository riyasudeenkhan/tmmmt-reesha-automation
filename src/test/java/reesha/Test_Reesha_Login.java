package reesha;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.Test;

import reesha.Reesha_Login;
import utility.TestBase;

public class Test_Reesha_Login extends TestBase {
	Reesha_Login LoginPage;
	RestaurantAdmin obj;
	Logger log = Logger.getLogger(Test_Reesha_Login.class);

	@BeforeClass
	public void classSetup() {
		/*
		 * log.info("Open reesha."); driver.get("https://abd.tmmmt.com");
		 */
		log.info("Initialize Reesha Login page");
 		LoginPage = PageFactory.initElements(driver, Reesha_Login.class);
 		obj = PageFactory.initElements(driver, RestaurantAdmin.class);
	}
 
	@Test //(priority=1)
	public void testLogin() {
		LoginPage.login_reesha("automation@tmmmt.com", "tmmmt123");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		log.info("Login Successfully, now it is the time to Log Off buddy.");
	}
	
	@Test
	public void testRestAdmin() {
		obj.navigateToRestAdmin();
		obj.openRestAdminDetails("herfy1");
		obj.activateRestAdmin();
		
		log.info("Retry Activating Restaurant");
		obj.navigateToRestAdmin();
		obj.openRestAdminDetails("herfy1");
		obj.activateRestAdmin();
	}
	
	//Forcefully failed this test as verify listener.		
	@Test		
	public void TestToFail()				
	{		
	    System.out.println("This method to test fail");					
	    Assert.assertTrue(false);			
	}	
 
	/*@AfterClass
	public void classCleanUp() {
		System.out.println("Successfully Completed Automation!!!");
	}*/
}
