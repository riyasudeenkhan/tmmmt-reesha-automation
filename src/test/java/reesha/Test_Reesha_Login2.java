package reesha;


import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import utility.TestBase;

public class Test_Reesha_Login2 extends TestBase {
	RestaurantAdmin obj;

	@BeforeMethod
	public void setup() {
		System.out.println("Initialize Reesha Login page");
 		obj = PageFactory.initElements(driver, RestaurantAdmin.class);
	}
 
	
	@Test
	public void testRestAdmin() {
		System.out.println("<<<<<<<<<<<<testRestAdmin>>>>>>>>>>>");
		obj.navigateToRestAdmin();
		obj.openRestAdminDetails("herfy1");
		obj.activateRestAdmin();
	}
 
	@Override
	@AfterMethod
	public void cleanUp() {
		System.out.println("Successfully Completed Automation!!!");
	}
}
