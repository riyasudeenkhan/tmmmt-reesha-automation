package reesha;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import reesha.RestaurantAdmin;
import utility.TestBase;

public class TestRestaurantAdmin extends TestBase {
	RestaurantAdmin restAdm;
	Logger log = Logger.getLogger(TestRestaurantAdmin.class);
	
	@Override
	public void init() {
		//driver.get("https://abd.tmmmt.com");
		log.info("Initialize RestaurantAdmin");
		//restAdm = PageFactory.initElements(driver, RestaurantAdmin.class);
	}
 
	@Test //(priority=1)
	public void testActivateRestAdmin() {
		log.info("Starts testActivateRestAdmin");
		restAdm = PageFactory.initElements(driver, RestaurantAdmin.class);
		restAdm.navigateToRestAdmin();
		restAdm.openRestAdminDetails("8000150318");
		Boolean isActive = restAdm.activateRestAdmin();
		if(!isActive) {
			log.error("Restaurant Activation has been failed.");
			Assert.assertTrue(false);
		}
		log.info(" Successfully Activated Restaurant Admin");
	}
 
	@Test
	public void testGotoRestaurantsPage() {
		restAdm.dummy();
		log.info("Navigation done.");
	}
	
	/*@AfterClass
	public void classCleanUp() {
		System.out.println("Successfully Completed Reesha Automation!!!");
 
	}*/
}
