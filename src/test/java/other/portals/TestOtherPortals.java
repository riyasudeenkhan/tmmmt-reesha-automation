package other.portals;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import utility.TestBase;

public class TestOtherPortals extends TestBase {
	Logger log = Logger.getLogger(TestOtherPortals.class);
	
	@Test
	public void isUpAndRunningTmmmtPortal() {
		log.info("is up and running tmmmt.com?");
		try {
			driver.get("https://tmmmt.com");
			driver.navigate().refresh();
			waitTime(5);
			log.debug("Change portal language into English");
			driver.findElement(By.xpath("//p[text()='EN']")).click();
			waitTime(5);
			driver.navigate().refresh();
			waitTime(5);
			log.debug("Goto Contack us page and check with the Submit button is present");
			driver.findElement(By.xpath("//p[text()='Contact us']")).click();
			waitTime(5);
			if(driver.findElement(By.id("btn")).isDisplayed())
				log.debug("TMMMT.com is up and running");
		 } catch(Exception ex) { 
			  log.debug("TMMMT.com is down!"); 
			  Assert.fail("Please check the web page is up and running"); 
		}
	}
	
	@Test
	public void isUpAndRunningSthatPortal() {
		log.info("is up and running sthat.com?");
		try {
			waitTime(3);
			driver.get("https://sthat.com");
			driver.navigate().refresh();
			waitTime(5);
			driver.findElement(By.xpath("//*[@id='u1774-4']/p")).click();	//Click English
			waitTime(5);
			driver.findElement(By.xpath("//*[@id='u2163-4']/p")).click();	//Click Feature
			waitTime(5);
			if(driver.findElement(By.xpath("//*[@id='u2143-4']/p")).isDisplayed()) {
				log.debug("goto Home > ");
				//driver.findElement(By.xpath("//*[@id='navbarSupportedContent']/ul/li[1]/a")).click();	//Click Home
				driver.navigate().back();
			}
			
			waitTime(5);
			driver.findElement(By.xpath("//*[@id='u2138-4']/p")).click();	//Click Contact Us
			waitTime(5);
			if(driver.findElement(By.xpath("//*[@id='btn']")).isDisplayed()) {  //Verify Submit button
				log.debug("goto Home > ");
				driver.navigate().back();
				log.debug("STHAT.com is up and running!!");
			}			
			
		  } catch(Exception ex) { 
			  log.debug("STHAT.com is down!"); 
			  Assert.fail("Please check the web page is up and running"); 
		}
		 
	}
	
	
	@Test
	public void isUpAndRunningTmmmtServices() {
		log.info("is up and running all TmmmT services?");
		try {
			waitTime(3);
			driver.get("http://health.tmmmt.com/");
			driver.navigate().refresh();
			waitTime(5);
			
			String status = driver.findElement(By.className("status-text")).getText();
			log.debug("Status = "+status);
			if(status.equalsIgnoreCase("All Up")) 
				log.debug("All the TmmmT services are up and running.");
			else
				throw new Exception("TmmmT Services are down");
		
		  } catch(Exception ex) { 
			  log.debug("TmmmT services are down! \n"+ex.getMessage()); 
			  Assert.fail("Please check that TmmmT services are up and running"); 
		  }
	}
	
	
	@Test
	public void isUpAndRunningSthatServices() {
		log.info("is up and running all STHAT services?");
		try {
			waitTime(3);
			driver.get("http://health.sthat.com/");
			driver.navigate().refresh();
			waitTime(5);
			
			String status = driver.findElement(By.className("status-text")).getText();
			if(status.equalsIgnoreCase("All Up")) 
				log.debug("All the STHAT services are up and running.");
			else
				throw new Exception("STHAT Services are down");
		
		  } catch(Exception ex) { 
			  log.debug("STHAT services are down! \n"+ex.getMessage()); 
			  Assert.fail("Please check STHAT services are up and running"); 
		  }	
	}
 
}
